//将军侍从胡斯

proc_start 1
var $item1
var $item2
var $item3
var $ask1
var $ask2

$item1 = havethisitem 勇气之魂
$item2 = havethisitem 智慧之魂
$item3 = havethisitem 力量之魂

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1

if $sw3 > 5399
	dlg 0 [{#FFFF9900=%s#}]你对将军的忠诚让我们感到无比的欣慰，请接受将军的祝福吧。 name
	UseSkill 1259 1
else
endif

if level < 34
	dlg 1 千年之前古战场中那场黄帝和炎帝的大战，死伤无数。那场战斗没有胜利者，所有将士都在此战中阵亡。我们当时守在谷外才得以幸免，但因当时战争杀戮太多，惊天动地，参与此次战争的人都受到了终生的诅咒，我们无法再离开古战场这个区域，当时的旧战场也因此而封闭。我们将军的魂魄在谷中回荡，他的精神力量还是非常强大的。
	return
else
	dlg 0 我们受到了千年的诅咒，无法进入旧古战场区域，也无法离开古战场，如果拥有足够的将军之魂，会让我们得以解脱，你能帮助我们得到足够的将军之魂吗？把它带来给我，我会让给睦卓大的力量，以示我对你的谢意。同一时间只能加有一种将军能力加强。
	$ask1 = ask 你想要得到是什么力量？ 将军之威 将军之灵 将军之风 离开
	if $ask1 == 0
		$ask2 = ask 需要一个勇气之魂和一个力量之魂，才可以得到将军之威。 需要 不需要
		if $ask2 == 0
			if $item1 > 0
				if $item3 > 0
					RemoveThisItem 勇气之魂	1
					RemoveThisItem 力量之魂	1
					UseSkill 1281 1
					return
				else
					dlg 1 你没有足够的力量之魂
					return
				endif
			else
				dlg 1 你没有足够的勇气之魂
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 1
		$ask2 = ask 需要一个勇气之魂和一个智慧之魂，才可以得到将军之灵。 需要 不需要
		if $ask2 == 0
			if $item1 > 0
				if $item2 > 0
					RemoveThisItem 勇气之魂	1
					RemoveThisItem 智慧之魂	1
					UseSkill 1282 1
					return
				else
					dlg 1 你没有足够的智慧之魂
					return
				endif
			else
				dlg 1 你没有足够的勇气之魂
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 2
		$ask2 = ask 需要一个力量之魂和一个智慧之魂，才可以得到将军之风。 需要 不需要
		if $ask2 == 0                 
			if $item2 > 0
				if $item3 > 0
					RemoveThisItem 力量之魂	1
					RemoveThisItem 智慧之魂	1
					UseSkill 1283 1
					return
				else
					dlg 1 你没有足够的力量之魂
					return
				endif
			else
				dlg 1 你没有足够的智慧之魂
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif
endif
proc_end