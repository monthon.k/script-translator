//将军侍从伍莲

proc_start 1
var $ask1

	dlg 0 [{#FFFF9900=%s#}]我需要有大量的人来帮助我们让将军解脱，如果有了足够的将军之魂，就可以召唤将军的灵魂，到时候帮助过我的人，都会得到你们的报酬的。 name
	$ask1 = ask 你想我帮你做什么？ 捐献 声望查询 将军声望说明 离开
	if $ask1 == 0
		jump 2
	else
	endif
	
	if $ask1 == 1
		jump 3
	else
	endif
	
	if $ask1 == 2
		dlg 0 只有得到将军氏族的认可，才能得到将军装备。将军声望被分为5个级别，分别为{#FFFFFF00=冷漠#}，{#FFFFFF00=陌生#}，{#FFFFFF00=熟识#}，{#FFFFFF00=友好#}，{#FFFFFF00=信任#}。
		dlg 0 每个声望级别分别需要的声望值是{#FFFFFF00=冷漠#}（0），{#FFFFFF00=陌生#}（200），{#FFFFFF00=熟识#}（600），{#FFFFFF00=友好#}（1800），{#FFFFFF00=信任#}（5400）。
		dlg 0 获得声望的方式很多，可以通过捐献{#FF00FF00=勇气之魂#}，{#FF00FF00=智慧之魂#}，{#FF00FF00=力量之魂#}，以上3种物品，每一个可以获得2点声望，而捐献{#FF00FF00=将军灵魂碎片#}这个物品每一个可以获得5点声望。还可以通过做古战场任务，召唤将军之魂等等方式来获得声望。
		dlg 1 在不同的声望能打造装备是不同的：\n{#FFFFFF00=冷漠#}可以打造：护腕\n{#FFFFFF00=陌生#}可以打造：鞋子\n{#FFFFFF00=熟识#}可以打造：头盔\n{#FFFFFF00=友好#}可以打造：肩膀和衣服\n{#FFFFFF00=信任#}可以打造：武器和盾牌
		return
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif

proc_end

proc_start 2
var $ask2

$item1 = havethisitem 勇气之魂
$item2 = havethisitem 智慧之魂
$item3 = havethisitem 力量之魂
$item4 = havethisitem 将军灵魂碎片

$ask2 = ask 你要捐献什么？ 勇气之魂 智慧之魂 力量之魂 将军灵魂碎片 离开
if $ask2 == 0
	if $item1 > 4
		if 2173 < 100
			RemoveThisItem 勇气之魂	5
			Talk 3000 0xffff0000 你的将军氏族声望增加了10点
			tell 你的将军氏族声望增加了10点
			//一级声望
			2172 += 10
			if 2172 > 99
				//二级声望
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 你的将军声望已经到了最高级别无法再提高了。
			return
		endif
	else
		dlg 1 勇气之魂一次最少捐献5个，你身上的勇气之魂不够。
		return
	endif
else
endif

if $ask2 == 1
	if $item2 > 4
		if 2173 < 100
			RemoveThisItem 智慧之魂	5
			Talk 3000 0xffff0000 你的将军氏族声望增加了10点
			tell 你的将军氏族声望增加了10点
			//一级声望
			2172 += 10
			if 2172 > 99
				//二级声望
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 你的将军声望已经到了最高级别无法再提高了。
			return
		endif
	else
		dlg 1 智慧之魂一次最少捐献5个，你身上的智慧之魂不够。
		return
	endif
else
endif

if $ask2 == 2
	if $item3 > 4
		if 2173 < 100
			RemoveThisItem 力量之魂	5
			Talk 3000 0xffff0000 你的将军氏族声望增加了10点
			tell 你的将军氏族声望增加了10点
			//一级声望
			2172 += 10
			if 2172 > 99
				//二级声望
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 你的将军声望已经到了最高级别无法再提高了。
			return
		endif
	else
		dlg 1 力量之魂一次最少捐献5个，你身上的力量之魂不够。
		return
	endif
else
endif

if $ask2 == 3
	if $item4 > 1
		if 2173 < 100
			RemoveThisItem 将军灵魂碎片	2
			Talk 3000 0xffff0000 你的将军氏族声望增加了10点
			tell 你的将军氏族声望增加了10点
			//一级声望
			2172 += 10
			if 2172 > 99
				//二级声望
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 你的将军声望已经到了最高级别无法再提高了。
			return
		endif
	else
		dlg 1 将军灵魂碎片一次最少捐献2个，你身上的将军灵魂碎片不够。
		return
	endif
else
endif

if $ask2 == 4
	return
else
endif
proc_end

proc_start 3
if 2173 < 0
	2173 = 100
else
endif
if 2173 > 100
	2173 = 100
else
endif

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1
if $sw3 > 5399
	dlg 1 [{#FFFF9900=%s#}]你现在的声望为{#FFFF9900=%s#}，与将军氏族的关系为{#FFFFFF00=信任#} name $sw3
	return
else
endif

if $sw3 > 1799
	dlg 1 [{#FFFF9900=%s#}]你现在的声望为{#FFFF9900=%s#}，与将军氏族的关系为{#FFFFFF00=友好#} name $sw3
	return
else
endif

if $sw3 > 599
	dlg 1 [{#FFFF9900=%s#}]你现在的声望为{#FFFF9900=%s#}，与将军氏族的关系为{#FFFFFF00=熟识#} name $sw3
	return
else
endif

if $sw3 > 199
	dlg 1 [{#FFFF9900=%s#}]你现在的声望为{#FFFF9900=%s#}，与将军氏族的关系为{#FFFFFF00=陌生#} name $sw3
	return
else
endif

if $sw3 > -1
	dlg 1 [{#FFFF9900=%s#}]你现在的声望为{#FFFF9900=%s#}，与将军氏族的关系为{#FFFFFF00=冷漠#} name $sw3
	return
else
endif
proc_end