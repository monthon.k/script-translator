//陶朱神匠严铸

proc_start 1
var $ask1
	$ask1 = ask 这世间没有我们陶朱不会打造的武器装备，即使是像古战场里面那种神器级别的金属我们都有办法锤炼，当然这是需要花费一些经费的,你需要打造什么？ 将军装备 神器套装 神器打造说明 离开
	if $ask1 == 0
		jump 2
	else
	endif
	
	if $ask1 == 1
		jump 3
	else
	endif
	
	if $ask1 == 2 
		dlg 0 在古战场中会掉落一种叫{#FF00FF00=被侵蚀的灵魂碎片#}的物品，这种物品是用来合成{#FF00FF00=将军套装#}的必备道具，得到这种物品之后可以使用{#FF00FF00=净魂之玉#}这种物品来净化它。
		dlg 0 净化之后有一定几率得到{#FF00FF00=将军灵魂碎片#}，使用这种碎片合成将军灵魂之后可以召唤古战场中的将军，打败他之后就能得到合成各职业套装的最终材料锭。
		dlg 1 拿着这些材料之后就可以到我这里来换取所需要的装备。
		return
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif
proc_end

proc_start 2
var $ask2
var $ask3
var $amount
var $rand1
var $item1
var $n
$n = GetNullItemCount
$item1 = havethisitem 将军灵魂碎片
var $item2
$item2 = havethisitem 将军战枪
var $item3
$item3 = havethisitem 将军战甲
var $item4
$item4 = havethisitem 将军铁肩
var $item5
$item5 = havethisitem 将军战盔
var $item6
$item6 = havethisitem 将军战靴
var $item7
$item7 = havethisitem 将军铁腕

	$ask2 = ask 你需要打造的是将军装备中的哪部分？ 将军战枪 将军战甲 将军铁肩 将军战盔 将军战靴 将军铁腕 将军灵魂 离开
	if $ask2 == 0
		$ask3 = ask 打造将军战枪需要200个将军灵魂碎片和20金，将军战枪分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 199
				if money > 19999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	200
						money -= 20000
						add_Item 将军战枪 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军战枪。
				return
			endif
		else
			if $item2 > 0
				$rand1 = rand 40
				$rand1 += 1
				RemoveThisItem 将军战枪	1
				add_Item 将军灵魂碎片 88
				add_Item 将军灵魂碎片 82
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军战枪。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 1
		$ask3 = ask 打造将军战甲需要180个将军灵魂碎片和18金，将军战甲分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 179
				if money > 17999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	180
						money -= 18000
						add_Item 将军战甲 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军战甲。
				return
			endif
		else
			if $item3 > 0
				$rand1 = rand 36
				$rand1 += 1
				RemoveThisItem 将军战甲	1
				add_Item 将军灵魂碎片 90
				add_Item 将军灵魂碎片 63
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军战甲。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 2
		$ask3 = ask 打造将军铁肩需要160个将军灵魂碎片和16金，将军铁肩分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 159
				if money > 15999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	160
						money -= 16000
						add_Item 将军铁肩 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军铁肩。
				return
			endif
		else
			if $item4 > 0
				$rand1 = rand 32
				$rand1 += 1
				RemoveThisItem 将军铁肩	1
				add_Item 将军灵魂碎片 80
				add_Item 将军灵魂碎片 56
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军铁肩。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 3
		$ask3 = ask 打造将军战盔需要140个将军灵魂碎片和14金，将军战盔分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 139
				if money > 13999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	140
						money -= 14000
						add_Item 将军战盔 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军战盔。
				return
			endif
		else
			if $item5 > 0
				$rand1 = rand 28
				$rand1 += 1
				RemoveThisItem 将军战盔	1
				add_Item 将军灵魂碎片 70
				add_Item 将军灵魂碎片 49
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军战盔。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 4
		$ask3 = ask 打造将军战靴需要120个将军灵魂碎片和12金，将军战靴分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 119
				if money > 11999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	120
						money -= 12000
						add_Item 将军战靴 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军战靴。
				return
			endif
		else
			if $item6 > 0
				$rand1 = rand 24
				$rand1 += 1
				RemoveThisItem 将军战靴	1
				add_Item 将军灵魂碎片 50
				add_Item 将军灵魂碎片 52
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军战靴。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 5
		$ask3 = ask 打造将军铁腕需要100个将军灵魂碎片和10金，将军铁腕分解将会得到一定数量的将军灵魂碎片 打造 分解
		if $ask3 == 0
			if $item1 > 99
				if money > 9999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	100
						money -= 10000
						add_Item 将军铁腕 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军铁腕。
				return
			endif
		else
			if $item7 > 0
				$rand1 = rand 20
				$rand1 += 1
				RemoveThisItem 将军铁腕	1
				add_Item 将军灵魂碎片 85
				add_Item 将军灵魂碎片 $rand1
				return
			else
				dlg 1 你身上没有将军铁腕。
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 6
		$ask3 = ask 打造将军灵魂需要300个将军灵魂碎片和30金才能合成，需要打造吗？ 打造 离开
		if $ask3 == 0
			if $item1 > 299
				if money > 29999
					if $n > 0
						RemoveThisItem 将军灵魂碎片	300
						money -= 30000
						add_Item 将军灵魂 1
						return
					else
						dlg 1 你的背包空间不够。
						return
					endif
				else
					dlg 1 你身上没有足够的金钱。
					return
				endif
			else
				dlg 1 你身上的将军灵魂碎片不够打造将军灵魂。
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask2 == 7
		return
	else
	endif
proc_end

proc_start 3
var $ask4
var $ask5
var $n
$n = GetNullItemCount
var $item2
$item2 = havethisitem 将军战枪
var $item3
$item3 = havethisitem 将军战甲
var $item4
$item4 = havethisitem 将军铁肩
var $item5
$item5 = havethisitem 将军战盔
var $item6
$item6 = havethisitem 将军战靴
var $item7
$item7 = havethisitem 将军铁腕
var $item8
$item8 = havethisitem 凝兵锭
var $item9
$item9 = havethisitem 凝甲锭
var $item10
$item10 = havethisitem 凝肩锭
var $item11
$item11 = havethisitem 凝盔锭
var $item12
$item12 = havethisitem 凝靴锭
var $item13
$item13 = havethisitem 凝腕锭

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1

	if Profession == 1
	$ask4 = ask 你需要打造的是将军装备中的哪部分？ 武器和盾牌 战魂铁甲 战魂铁肩 战魂铁盔 战魂铁靴 战魂铁腕 离开
		if $ask4 == 0
			if $sw3 > 5399
			$ask5 = ask 你想打造的武器和防具是什么？ 凝血剑（双手剑） 蓝鳞剑（单手剑） 将仙盾（盾牌） 离开
				if $ask5 == 0
					dlg 1 打造凝血剑需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 凝血剑 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 打造蓝鳞剑需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 蓝鳞剑 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 2
					dlg 1 打造将仙盾需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 将仙盾 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 3
					return
				else
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 打造战魂铁甲需要凝甲锭和将军铁甲。
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem 凝甲锭 1
							RemoveThisItem 将军战甲	1
							add_Item 战魂铁甲 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战甲这个材料。
						return
					endif
				else
					dlg 1 你缺少凝甲锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 打造战魂铁肩需要凝肩锭和将军铁肩。
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem 凝肩锭 1
							RemoveThisItem 将军铁肩	1
							add_Item 战魂铁肩 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁肩这个材料。
						return
					endif
				else
					dlg 1 你缺少凝肩锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 599
				dlg 1 打造战魂铁盔需要凝盔锭和将军战盔。
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem 凝盔锭 1
							RemoveThisItem 将军战盔	1
							add_Item 战魂铁盔 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战盔这个材料。
						return
					endif
				else
					dlg 1 你缺少凝盔锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 199
				dlg 1 打造战魂铁靴需要凝靴锭和将军战靴。
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem 凝靴锭 1
							RemoveThisItem 将军战靴	1
							add_Item 战魂铁靴 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战靴这个材料。
						return
					endif
				else
					dlg 1 你缺少凝靴锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 打造战魂铁腕需要凝腕锭和将军铁腕。
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem 凝腕锭 1
							RemoveThisItem 将军铁腕	1
							add_Item 战魂铁腕 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁腕这个材料。
						return
					endif
				else
					dlg 1 你缺少凝腕锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
	
	if Profession == 0
	$ask4 = ask 你需要打造的是将军装备中的哪部分？ 武器 侠客皮甲 侠客皮肩 侠客皮盔 侠客皮靴 侠客皮腕 离开
		if $ask4 == 0
			if $sw3 > 5399
			$ask5 = ask 你想要打造的武器是什么？ 血雾刃（匕首） 刺骨爪（爪子） 离开
				if $ask5 == 0
					dlg 1 打造血雾刃需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 血雾刃 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 打造刺骨爪需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 刺骨爪 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif 
				
				if $ask5 == 2
					return
				else
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 打造侠客皮甲需要凝甲锭和战魂铁甲。
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem 凝甲锭 1
							RemoveThisItem 将军战甲	1
							add_Item 侠客皮甲 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战甲这个材料。
						return
					endif
				else
					dlg 1 你缺少凝甲锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 打造侠客皮肩需要凝肩锭和将军铁肩。
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem 凝肩锭 1
							RemoveThisItem 将军铁肩	1
							add_Item 侠客皮肩 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁肩这个材料。
						return
					endif
				else
					dlg 1 你缺少凝肩锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 599
				dlg 1 打造侠客皮盔需要凝盔锭和将军战盔。
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem 凝盔锭 1
							RemoveThisItem 将军战盔	1
							add_Item 侠客皮盔 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战盔这个材料。
						return
					endif
				else
					dlg 1 你缺少凝盔锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 199
				dlg 1 打造侠客皮靴需要凝靴锭和将军战靴。
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem 凝靴锭 1
							RemoveThisItem 将军战靴	1
							add_Item 侠客皮靴 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战靴这个材料。
						return
					endif
				else
					dlg 1 你缺少凝靴锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 打造侠客皮腕需要凝腕锭和将军铁腕。
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem 凝腕锭 1
							RemoveThisItem 将军铁腕	1
							add_Item 侠客皮腕 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁腕这个材料。
						return
					endif
				else
					dlg 1 你缺少凝腕锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
	
	if Profession == 3
	$ask4 = ask 你需要打造的是将军装备中的哪部分？ 法杖 临天法袍 临天法肩 临天法帽 临天法靴 临天法腕 离开
		if $ask4 == 0
			if $sw3 > 5399
				$ask5 = ask 你想要打造的法杖是什么？ 古枫法杖（火） 神灵法杖（冰） 离开
				if $ask5 == 0
					dlg 1 打造古枫法杖需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 古枫法杖 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 打造神灵法杖需要凝兵锭和将军战枪。
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem 凝兵锭 1
								RemoveThisItem 将军战枪	1
								add_Item 神灵法杖 1
								return
							else
								dlg 1 你的背包空间不够。
								return
							endif
						else
							dlg 1 你缺少将军战枪这个材料。
							return
						endif
					else
						dlg 1 你缺少凝兵锭这个材料。
						return
					endif
				else
				endif
				
				if $ask5 == 2
					return
				else
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 打造临天法袍需要凝甲锭和战魂铁甲。
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem 凝甲锭 1
							RemoveThisItem 将军战甲	1
							add_Item 临天法袍 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战甲这个材料。
						return
					endif
				else
					dlg 1 你缺少凝甲锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 打造临天法肩需要凝肩锭和将军铁肩。
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem 凝肩锭 1
							RemoveThisItem 将军铁肩	1
							add_Item 临天法肩 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁肩这个材料。
						return
					endif
				else
					dlg 1 你缺少凝肩锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 1799
				dlg 1 打造临天法帽需要凝盔锭和将军战盔。
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem 凝盔锭 1
							RemoveThisItem 将军战盔	1
							add_Item 临天法帽 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战盔这个材料。
						return
					endif
				else
					dlg 1 你缺少凝盔锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 599
				dlg 1 打造临天法靴需要凝靴锭和将军战靴。
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem 凝靴锭 1
							RemoveThisItem 将军战靴	1
							add_Item 临天法靴 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军战靴这个材料。
						return
					endif
				else
					dlg 1 你缺少凝靴锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 打造临天法腕需要凝腕锭和将军铁腕。
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem 凝腕锭 1
							RemoveThisItem 将军铁腕	1
							add_Item 临天法腕 1
							return
						else
							dlg 1 你的背包空间不够。
							return
						endif
					else
						dlg 1 你缺少将军铁腕这个材料。
						return
					endif
				else
					dlg 1 你缺少凝腕锭这个材料。
					return
				endif
			else
				dlg 1 你的声望不够，无法锻造。
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
proc_end