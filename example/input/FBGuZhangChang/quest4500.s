proc_start 1
	var $v1
	var $ask1
	
	$v1 = Can_get_new_quest
	if $v1 == 0
		dlg 1 你接的任务太多了!
		NpcStartMove
		return
	else
	endif
		
	dlg 0 听前几天去过古战场的勇士说神坛六祖发明的净魂之玉可以净化一切污浊的物品，就觉得，县民染上了污浊之物才会得不治之症，净魂之玉或许可以净化驱赶病魔。
	$ask1 = ask 古战场山谷门口的陶朱商人铁利，制作了净化山谷中冤魂的净化晶石——{#FFFF0000=净魂之玉#}。你前去找他买一个带回来给我，我好马上治疗病人！ 接受 拒绝
	if $ask1 == 0
		set_quest_state 4500 1
		NpcStartMove
		return
  else
	NpcStartMove
  endif
proc_end

proc_start 2
	var $ask2
	dlg 0 郎中袁远？他说我的{#FFFF0000=净魂之玉#}能净化驱赶病魔？哈哈，这可是连我自己都不能确定的事呢！但我能很肯定这些石头能逐渐化解在古战场战死的冤魂的怨气。
	$ask2 = ask 既然他愿意试试这{#FFFF0000=净魂之玉#}的神奇力量，你就花上{#FFFF0000=500铜#}买了带去给他吧！ 购买 拒绝
	if $ask2 == 0
		if money > 499
			money -= 500
			add_Item 净魂之玉 1
			set_quest_state 4500 2
			return
		else
			dlg 1 你身上没有足够的钱。
			return
		endif
  else
  endif		
proc_end

proc_start 3
	var $item1
	var $v2
	$item1 = havethisitem 净魂之玉
	$v2 = Get_quest_state 4500
	if $v2 == 2
		if $item1 > 0
			dlg 1 恩……这{#FFFF0000=净魂之玉#}透着神奇的力量。
			removethisitem 净魂之玉 1
			done_quest 4500
			NpcStartMove
			return
		else
			dlg 1 赶紧去古战场陶朱商人铁利那买{#FFFF0000=净魂之玉#}回来！
			NpcStartMove
			return
		endif
	else
		dlg 1 赶紧去古战场陶朱商人铁利那买{#FFFF0000=净魂之玉#}回来！
		NpcStartMove
		return
	endif
proc_end
