//神将意灵
proc_start 1
	var $v1
	var $ask1
	
	$v1 = Can_get_new_quest
	if $v1 == 0
		dlg 1 你接的任务太多了!
		return
	else
	endif
		
	dlg 0 为了净化山谷里的冤魂，特别制作了净化晶石，用这些石头就能逐渐的化解在古战场战死的冤魂的怨气。而对于那个神秘恐怖的神将意灵的怨灵，能否消除其被魔化的深深的怨气，对此也是十分忧虑……
	$ask1 = ask 神将意灵是运用{#FFFF0000=将军灵魂#}在将军侍从卜平那召唤出来的怨灵。怨灵被轩辕神族所禁锢封闭，却始终无法消除被魔化的怨气。成功召唤神将意灵将获得轩辕族声望。 接受 拒绝
	if $ask1 == 0
		set_quest_state 4503 1
		return
  else
  endif
proc_end

proc_start 2
		if 101 > 0
			dlg 1 你成功召唤了神将意灵，轩辕声望已有所增加。当轩辕声望达到一定等级，可以在陶朱神匠齐绮处打造将军装备。
			done_quest 4503
			return
		else
			dlg 1 神将意灵是运用{#FFFF0000=将军灵魂#}在将军侍从卜平那召唤出来的怨灵。
			return
		endif
proc_end