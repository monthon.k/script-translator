//General^attendants

proc_start 0
	relate_quest 4456 0 1
	relate_quest 4456 1 2
	relate_quest 4501 0 1
	relate_quest 4501 2 3
	relate_quest 4502 0 1
	relate_quest 4502 1 2
	relate_quest 4503 0 1
	relate_quest 4503 1 2
	relate_quest 4504 1 2
proc_end

proc_start 1
	var $sqd
	$sqd = ShowQuestDlg Warriors^of^the^ancient^battlefield^is^very^powerful,^people^need^to^be^very^careful.
	if $sqd == -1
		dlg 1 Those^Warriors^of^the^ancient^battlefield^can^no^longer^control^themselves,^they^don't^want^to^kill^it,^the^curse^is^so^vicious,^I^do^not^know^when^they^will^be^freed.
		return
	else
	endif
proc_end
