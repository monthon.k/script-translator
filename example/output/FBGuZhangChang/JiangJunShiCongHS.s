//General^Chamberlain^Hoss

proc_start 1
var $item1
var $item2
var $item3
var $ask1
var $ask2

$item1 = havethisitem The^soul^of^courage
$item2 = havethisitem Spirit^of^wisdom
$item3 = havethisitem Power^of^soul

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1

if $sw3 > 5399
	dlg 0 [{#FFFF9900=%s#}]^let^you^on^the^General's^loyalty^was^a^great^relief,^please^accept^the^General's^blessing. name
	UseSkill 1259 1
else
endif

if level < 34
	dlg 1 Thousands^of^years^ago^the^ancient^battlefield^in^the^war^of^Huangdi^and^yandi,^killing^hundreds.^Battle^without^winners,^all^the^soldiers^killed^in^this^war.^We^were^kept^outside^the^Valley^was^able^to^escape,^but^due^to^war^killing^so^many,^flashy,^people^involved^in^the^war^have^been^cursed^for^life,^we^can^no^longer^leave^the^ancient^battlefield^in^the^region,^when^the^old^battlefields^is^closed.^Our^soul^General^echoed^through^the^Valley,^his^mental^strength^is^very^strong.
	return
else
	dlg 0 We^were^a^thousand-year^curse,^unable^to^enter^the^old^ancient^battlefield^area,^unable^to^leave^the^battlefield,^if^you^have^enough^general^spirit,^will^make^us^free,^can^you^help^us^get^enough^of^the^general^spirit^of^it?^Bring^it^to^me,^I'll^let^Mu^strength,^to^show^my^gratitude^to^you.^Only^added^a^general^capability^to^strengthen^at^the^same^time.
	$ask1 = ask You^want^what? General^powers General^spirit General^wind Depart
	if $ask1 == 0
		$ask2 = ask One^needs^a^spirit^of^courage^and^the^power^of^the^soul^can^only^be^general^Wei. Need Do^not^need
		if $ask2 == 0
			if $item1 > 0
				if $item3 > 0
					RemoveThisItem The^soul^of^courage 1
					RemoveThisItem Power^of^soul 1
					UseSkill 1281 1
					return
				else
					dlg 1 You^do^not^have^enough^Power^of^soul
					return
				endif
			else
				dlg 1 You^don't^have^The^soul^of^courage
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 1
		$ask2 = ask Need^a^spirit^of^courage^and^wisdom,^the^spirit^of^the^General^can^only^be^spirit. Need Do^not^need
		if $ask2 == 0
			if $item1 > 0
				if $item2 > 0
					RemoveThisItem The^soul^of^courage 1
					RemoveThisItem Spirit^of^wisdom 1
					UseSkill 1282 1
					return
				else
					dlg 1 You^don't^have^the^Spirit^of^wisdom
					return
				endif
			else
				dlg 1 You^don't^have^The^soul^of^courage
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 2
		$ask2 = ask Need^a^Power^of^soul^and^a^spirit^of^wisdom,^can^only^be^General^in^the^breeze. Need Do^not^need
		if $ask2 == 0                 
			if $item2 > 0
				if $item3 > 0
					RemoveThisItem Power^of^soul 1
					RemoveThisItem Spirit^of^wisdom 1
					UseSkill 1283 1
					return
				else
					dlg 1 You^do^not^have^enough^Power^of^soul
					return
				endif
			else
				dlg 1 You^don't^have^the^Spirit^of^wisdom
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif
endif
proc_end
