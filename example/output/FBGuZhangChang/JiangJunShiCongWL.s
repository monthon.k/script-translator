//General^Chamberlain^Wu^Lian

proc_start 1
var $ask1

	dlg 0 [{#FFFFCC00=%s#}]^I^need^to^have^a^large^number^of^people^to^help^us^get^the^General^relief,^if^there^is^enough^general^spirit,^you^can^call^General's^soul,^then^people^who^helped^me,^you^will^be^paid. name
	$ask1 = ask What^do^you^want^me^to^do^for^you? Donation Standing^query General^reputation^description Depart
	if $ask1 == 0
		jump 2
	else
	endif
	
	if $ask1 == 1
		jump 3
	else
	endif
	
	if $ask1 == 2
		dlg 0 Only^be^recognized^by^General^clan^to^get^the^General^equipment.^General^reputation^is^divided^into^5^levels,^{#FFFFFF00=indifference#}^,{#FFFFFF00=^strange^#},{#FFFFFF00=^know^#},{#FFFFFF00=^friendly#}^and^,{#FFFFFF00=trust#}.
		dlg 0 Each^reputation^level^require^the^prestige^value^of^indifference^is^{#FFFFFF00=indifference^(0)#}^{#FFFFFF00=^strange^(200)#},^{#FFFFFF00=know^(600)#},^{#FFFFFF00=friendly^(1800)#}^,^{#FFFFFF00=trust^(5400)#}.
		dlg 0 Lots^of^ways^to^gain^popularity,^you^can^donate^{#FF00FF00=The^soul^of^courage#},{#FF00FF00=Spirit^of^wisdom#},{#FF00FF00=Power^of^soul#},^the^above^3^items,^each^one^can^gain^2^prestigious^and^donate^{#FF00FF00=General^soul^fragments#}^the^prestige^items^each^will^receive^5^points.^Can^also^do^old^tasks,^called^spirit^of^the^general^way^to^get^fame.
		dlg 1 Reputation^in^different^building^equipment^is^different:^\n{#FFFFFF00=indifference#}^can^create:^wrist^\n{#FFFFFF00=strange^#}^can^create:^shoes^\n{#FFFFFF00=know#}^can^create:^helmet^\n{#FFFFFF00=friendly#}^can^create:^shoulders^and^clothes^\n{#FFFFFF00=trust#}^to^create^weapons^and^shields
		return
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif

proc_end

proc_start 2
var $ask2

$item1 = havethisitem The^soul^of^courage
$item2 = havethisitem Spirit^of^wisdom
$item3 = havethisitem Power^of^soul
$item4 = havethisitem General^soul^fragments

$ask2 = ask What^do^you^want^to^donate? The^soul^of^courage Spirit^of^wisdom Power^of^soul General^soul^fragments Depart
if $ask2 == 0
	if $item1 > 4
		if 2173 < 100
			RemoveThisItem The^soul^of^courage 5
			Talk 3000 0xffff0000 Your^General^clan^prestige^increased^by^10^points
			tell Your^General^clan^prestige^increased^by^10^points
			//Level^of^prestige
			2172 += 10
			if 2172 > 99
				//Second-level^prestige
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 General^reputation^has^reached^the^highest^level^you^can^no^longer^increase.
			return
		endif
	else
		dlg 1 Soul^of^courage^a^5^minimum^donation,^your^spirit^of^courage^is^not^enough.
		return
	endif
else
endif

if $ask2 == 1
	if $item2 > 4
		if 2173 < 100
			RemoveThisItem Spirit^of^wisdom 5
			Talk 3000 0xffff0000 Your^General^clan^prestige^increased^by^10^points
			tell Your^General^clan^prestige^increased^by^10^points
			//Level^of^prestige
			2172 += 10
			if 2172 > 99
				//Second-level^prestige
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 General^reputation^has^reached^the^highest^level^you^can^no^longer^increase.
			return
		endif
	else
		dlg 1 5^wisdom^the^soul^of^a^minimum^donation,^your^spirit^of^wisdom^is^not^enough.
		return
	endif
else
endif

if $ask2 == 2
	if $item3 > 4
		if 2173 < 100
			RemoveThisItem Power^of^soul 5
			Talk 3000 0xffff0000 Your^General^clan^prestige^increased^by^10^points
			tell Your^General^clan^prestige^increased^by^10^points
			//Level^of^prestige
			2172 += 10
			if 2172 > 99
				//Second-level^prestige
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 General^reputation^has^reached^the^highest^level^you^can^no^longer^increase.
			return
		endif
	else
		dlg 1 Soul^is^a^minimum^donation^of^5,^the^soul^of^your^power^is^not^enough.
		return
	endif
else
endif

if $ask2 == 3
	if $item4 > 1
		if 2173 < 100
			RemoveThisItem General^soul^fragments 2
			Talk 3000 0xffff0000 Your^General^clan^prestige^increased^by^10^points
			tell Your^General^clan^prestige^increased^by^10^points
			//Level^of^prestige
			2172 += 10
			if 2172 > 99
				//Second-level^prestige
				2172 = 0
				2173 += 1
			else
			endif
		else
			dlg 1 General^reputation^has^reached^the^highest^level^you^can^no^longer^increase.
			return
		endif
	else
		dlg 1 General^soul^2^minimum^donation,^pieces,^you^don't^have^enough^General^pieces^of^soul.
		return
	endif
else
endif

if $ask2 == 4
	return
else
endif
proc_end

proc_start 3

if 2173 < 0
	2173 = 100
else
endif
if 2173 > 100
	2173 = 100
else
endif

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1
if $sw3 > 5399
	dlg 1 [{#FFFFCC00=%s#}]^your^reputation^now^is^{#FFFFCC00=%s#},^and^the^General^relationship^between^clans^as^{#FFFFFF00=trust^#} name $sw3
	return
else
endif

if $sw3 > 1799
	dlg 1 [{#FFFFCC00=%s#}]^your^reputation^now^is^{#FFFFCC00=%s#},^and^the^General^relationship^between^clans^as^{#FFFFFF00=friendly#} name $sw3
	return
else
endif

if $sw3 > 599
	dlg 1 [{#FFFFCC00=%s#}]^your^reputation^now^is^{#FFFFCC00=%s#},^relationship^with^the^generals^clan^to^{#FFFFFF00=know#} name $sw3
	return
else
endif

if $sw3 > 199
	dlg 1 [{#FFFFCC00=%s#}]^your^reputation^now^is^{#FFFFCC00=%s#},^relationship^with^the^generals^clan^to^{#FFFFFF00=strange#} name $sw3
	return
else
endif

if $sw3 > -1
	dlg 1 [{#FFFFCC00=%s#}]^your^reputation^now^is^{#FFFFCC00=%s#},^and^the^General^relationship^between^clans^as^{#FFFFFF00=indifference#} name $sw3
	return
else
endif
proc_end
