//Soul^NET^jade

proc_start 1
var $item1
var $item2
var $rand1
$rand1 = rand 4

$item1 = havethisitem Soul^NET^jade
$item2 = havethisitem Eroded^shards

if $rand1 == 0
	if $item1 > 0
		if $item2 > 0
			RemoveThisItem Soul^NET^jade 1
			RemoveThisItem Eroded^shards 1
			add_Item General^soul^fragments 1
		else
			dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
			return
		endif
	else
		dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
		return
	endif
else
endif

if $rand1 == 1
	if $item1 > 0
		if $item2 > 0
			RemoveThisItem Soul^NET^jade 1
			RemoveThisItem Eroded^shards 1
			add_Item The^soul^of^courage 1
		else
			dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
			return
		endif
	else
		dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
		return
	endif
else
endif

if $rand1 == 2
	if $item1 > 0
		if $item2 > 0
			RemoveThisItem Soul^NET^jade 1
			RemoveThisItem Eroded^shards 1
			add_Item Spirit^of^wisdom 1
		else
			dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
			return
		endif
	else
		dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
		return
	endif
else
endif

if $rand1 == 3
	if $item1 > 0
		if $item2 > 0
			RemoveThisItem Soul^NET^jade 1
			RemoveThisItem Eroded^shards 1
			add_Item Power^of^soul 1
			return
		else
			dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
			return
		endif
	else
		dlg 1 You^don't^have^enough^of^the^erosion^of^the^soul^fragments
		return
	endif
else
endif
proc_end
