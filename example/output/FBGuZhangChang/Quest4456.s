//Save^the^war^spirit

proc_start 1
	var $c
	$c = Can_get_new_quest
	if $c == 0
		dlg 1 Tasks^that^you^are^too^much!
		return
	else
	endif
	var $ask
	
	if level < 34
		dlg 1 Not^suitable^for^your^weak^stay^here,^leave^early^is^good^for^you.
		return
	else
		$ask = ask I^don't^want^someone^who^has^a^general^effect^of^soldiers^killing^again,^please^help^them^free. Accept Refuse
		if $ask == 0
			set_quest_state 4456 1
			return
		else
			return
		endif
	endif
proc_end

proc_start 2
	if 2174 > 9
		if 2175 > 9
			dlg 1 They^are^free^at^last,^thank^you^for^all^you^do^for^them.
			if 2173 < 100
				2172 += 10
				if 2172 > 99
				//Second-level^prestige
					2172 = 0
					2173 += 1
				else
				endif
				
				2172 += 10
				if 2172 > 99
				//Second-level^prestige
					2172 = 0
					2173 += 1
				else
				endif
				
				2172 += 10
				if 2172 > 99
				//Second-level^prestige
					2172 = 0
					2173 += 1
				else
				endif
				talk 3000 0xffff0000 Your^General^clan^prestige^increased^by^30^points
				tell Your^General^clan^prestige^increased^by^30^points
			else
				tell General^reputation^has^reached^the^highest^level^you^can^no^longer^increase.
			endif
			done_quest 4456
			2174 = 0
			2175 = 0
		else
			dlg 1 You^have^to^help^those^soldiers^out^of^the^curse.
			return
		endif
	else
		dlg 1 You^have^to^help^those^soldiers^out^of^the^curse.
		return
	endif
proc_end
