//Tao^Zhushen^casting

proc_start 1
var $ask1
	$ask1 = ask This^world^is^not^our^taozhu^will^not^build^weapons,^even^as^ancient^battlefields^that^artifact^level^metal^that^we^all^have^a^way^to^temper,^of^course,^takes^money,^what^you^need^to^build? General^equipment Artifact^set Artifact^build^instructions Depart
	if $ask1 == 0
		jump 2
	else
	endif
	
	if $ask1 == 1
		jump 3
	else
	endif
	
	if $ask1 == 2 
		dlg 0 Will^drop^a^call^in^the^ancient^battlefield^{#FF00FF00=^the^erosion^of^soul^debris^items#},^such^items^were^used^to^{#FF00FF00=General^synthesis#}^set^of^must-have^items,^such^items^can^use^{#FF00FF00=NET^soul^jade#}^this^article^to^purify^it.
		dlg 0 After^purification^has^a^chance^to^be^{#FF00FF00=General^soul^fragments#},^General^synthesis^of^this^debris^can^be^called^after^the^ancient^battlefield^of^the^soul^generals,^after^defeating^him^you^can^get^synthetic^suit^the^final^ingot^material.
		dlg 1 Can^take^these^materials^to^me^in^Exchange^for^needed^equipment.
		return
	else
	endif
	
	if $ask1 == 3
		return
	else
	endif
proc_end

proc_start 2
var $ask2
var $ask3
var $amount
var $rand1
var $item1
var $n
$n = GetNullItemCount
$item1 = havethisitem General^soul^fragments
var $item2
$item2 = havethisitem General^war^gun
var $item3
$item3 = havethisitem General^mail
var $item4
$item4 = havethisitem General^iron^shoulders
var $item5
$item5 = havethisitem General^war^helmet
var $item6
$item6 = havethisitem General^combat^boots
var $item7
$item7 = havethisitem General^iron^fist

	$ask2 = ask You^need^to^make^is^which^part^of^General^equipment? General^war^gun General^mail General^iron^shoulders General^war^helmet General^combat^boots General^iron^fist Soul^generals Depart
	if $ask2 == 0
		$ask3 = ask Create^General^General^gun^needs^200^pieces^of^soul^and^20^gold,^General^war^guns^breakdown^will^get^a^certain^number^of^General^pieces^of^soul Build Decomposition
		if $ask3 == 0
			if $item1 > 199
				if money > 19999
					if $n > 0
						RemoveThisItem General^soul^fragments 200
						money -= 20000
						add_Item General^war^gun 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^General^soul^fragments^you^build^enough^guns.
				return
			endif
		else
			if $item2 > 0
				$rand1 = rand 40
				$rand1 += 1
				RemoveThisItem General^war^gun 1
				add_Item General^soul^fragments 88
				add_Item General^soul^fragments 82
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 You^have^no^general^war.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 1
		$ask3 = ask General^General^build^armor^requires^180^pieces^of^soul^and^18^gold,^Shogun^armor^General^decomposition^will^get^a^certain^number^of^shards Build Decomposition
		if $ask3 == 0
			if $item1 > 179
				if money > 17999
					if $n > 0
						RemoveThisItem General^soul^fragments 180
						money -= 18000
						add_Item General^mail 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^General^soul^fragments^you^build^enough^armor.
				return
			endif
		else
			if $item3 > 0
				$rand1 = rand 36
				$rand1 += 1
				RemoveThisItem General^mail 1
				add_Item General^soul^fragments 90
				add_Item General^soul^fragments 63
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 You^have^no^junk^mail.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 2
		$ask3 = ask General^build^iron^shoulder^general^need^160^pieces^of^soul^and^16^gold,^General^iron^shoulder^General^decomposition^will^get^a^certain^number^of^shards Build Decomposition
		if $ask3 == 0
			if $item1 > 159
				if money > 15999
					if $n > 0
						RemoveThisItem General^soul^fragments 160
						money -= 16000
						add_Item General^iron^shoulders 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^General^soul^fragments^you^build^enough^iron^shoulders.
				return
			endif
		else
			if $item4 > 0
				$rand1 = rand 32
				$rand1 += 1
				RemoveThisItem General^iron^shoulders 1
				add_Item General^soul^fragments 80
				add_Item General^soul^fragments 56
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 You^have^no^general^iron^shoulders.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 3
		$ask3 = ask Create^General^General^war^helmets^need^140^shards^and^14^gold,^General^war^helmets^General^decomposition^will^get^a^certain^number^of^shards Build Decomposition
		if $ask3 == 0
			if $item1 > 139
				if money > 13999
					if $n > 0
						RemoveThisItem General^soul^fragments 140
						money -= 14000
						add_Item General^war^helmet 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^soul^fragments^you^build^enough^general^war^helmets.
				return
			endif
		else
			if $item5 > 0
				$rand1 = rand 28
				$rand1 += 1
				RemoveThisItem General^war^helmet 1
				add_Item General^soul^fragments 70
				add_Item General^soul^fragments 49
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 You^have^no^general^war^helmets.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 4
		$ask3 = ask Create^General^General^boots^need^120^pieces^of^soul^and^12^gold,^General^decomposition^boots^will^get^a^certain^number^of^General^pieces^of^soul Build Decomposition
		if $ask3 == 0
			if $item1 > 119
				if money > 11999
					if $n > 0
						RemoveThisItem General^soul^fragments 120
						money -= 12000
						add_Item General^combat^boots 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^General^soul^fragments^you^build^enough^boots.
				return
			endif
		else
			if $item6 > 0
				$rand1 = rand 24
				$rand1 += 1
				RemoveThisItem General^combat^boots 1
				add_Item General^soul^fragments 50
				add_Item General^soul^fragments 52
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 General^you^have^no^boots.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 5
		$ask3 = ask Create^General^General^needs^100^soul^fragments^and^10^gold^with^an^iron^fist,^General^General^decomposition^of^an^iron^fist^will^get^a^certain^number^of^pieces^of^soul Build Decomposition
		if $ask3 == 0
			if $item1 > 99
				if money > 9999
					if $n > 0
						RemoveThisItem General^soul^fragments 100
						money -= 10000
						add_Item General^iron^fist 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^soul^fragments^you^build^enough^generals^with^an^iron^fist.
				return
			endif
		else
			if $item7 > 0
				$rand1 = rand 20
				$rand1 += 1
				RemoveThisItem General^iron^fist 1
				add_Item General^soul^fragments 85
				add_Item General^soul^fragments $rand1
				return
			else
				dlg 1 You^have^no^general^with^an^iron^fist.
				return
			endif
		endif
	else
	endif
	
	if $ask2 == 6
		$ask3 = ask Create^the^soul^generals^need^100^General^synthesis^of^soul^fragments^and^30^payments^to,^do^I^need^to^create? Build Depart
		if $ask3 == 0
			if $item1 > 99
				if money > 29999
					if $n > 0
						RemoveThisItem General^soul^fragments 100
						money -= 30000
						add_Item Soul^generals 1
						return
					else
						dlg 1 You^don't^have^enough^bag^space.
						return
					endif
				else
					dlg 1 You^do^not^have^enough^money.
					return
				endif
			else
				dlg 1 General^General^soul^fragments^you^build^enough^souls.
				return
			endif
		else
			return
		endif
	else
	endif
	
	if $ask2 == 7
		return
	else
	endif
proc_end

proc_start 3
var $ask4
var $ask5
var $n
$n = GetNullItemCount
var $item2
$item2 = havethisitem General^war^gun
var $item3
$item3 = havethisitem General^mail
var $item4
$item4 = havethisitem General^iron^shoulders
var $item5
$item5 = havethisitem General^war^helmet
var $item6
$item6 = havethisitem General^combat^boots
var $item7
$item7 = havethisitem General^iron^fist
var $item8
$item8 = havethisitem Gel^tablets
var $item9
$item9 = havethisitem Gel^nail^ingot
var $item10
$item10 = havethisitem Frozen^Shoulder^tabs
var $item11
$item11 = havethisitem Gel^helmet^ingot
var $item12
$item12 = havethisitem Clotting^boots^ingot
var $item13
$item13 = havethisitem Gel^Wrist^ingot

var $sw1
$sw1 = GetPlayerVar -1 2172
var $sw2
$sw2 = GetPlayerVar -1 2173
var $sw3
$sw3 = $sw2
$sw3 *= 100
$sw3 += $sw1

	if Profession == 1
	$ask4 = ask You^need^to^make^is^which^part^of^General^equipment? Weapons^and^shields Spirit^armor Soul^iron^shoulders Soul^iron^helmet Soul^of^fight^iron^boots Soul^with^an^iron^fist Depart
		if $ask4 == 0
			if $sw3 > 5399
			$ask5 = ask What^do^you^want^to^build^weapons^and^armor^are? Blood^sword^(two-handed^sword) Blue^scale^swords^(one-handed^sword) Fairy^shield^(shield) Depart
				if $ask5 == 0
					dlg 1 Create^coagulation^coagulation^sword^need^tablets^and^General^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Blood^sword 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 Making^ingot^of^blue^scales^sword^need^to^pour^soldiers^and^generals^fighting^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Blue^scale^sword 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 2
					dlg 1 Create^fairy^shields^needs^to^gel^tablets^and^the^Junk^guns.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Fairy^shield 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 3
					return
				else
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 Create^the^soul^armor^requires^ingots^and^coagulation^General^armor.
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem Gel^nail^ingot 1
							RemoveThisItem General^mail 1
							add_item_byid 28004 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^armor^materials.
						return
					endif
				else
					dlg 1 Coagulation^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 Create^soul^need^iron^shoulder^Frozen^Shoulder^tabs^and^General^iron^shoulders.
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem Frozen^Shoulder^tabs 1
							RemoveThisItem General^iron^shoulders 1
							add_Item Soul^iron^shoulders 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 General^you^lack^iron^shoulder^the^material.
						return
					endif
				else
					dlg 1 Frozen^Shoulder^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 599
				dlg 1 Create^a^soul^of^iron^ingots^and^the^General^war^helmets^helmets^need^to^gel^helmets.
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem Gel^helmet^ingot 1
							RemoveThisItem General^war^helmet 1
							add_Item Soul^iron^helmet 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^lack^of^General^war^helmet^of^the^material.
						return
					endif
				else
					dlg 1 You^gel^helmet^ingot^is^missing^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 199
				dlg 1 Create^a^soul^of^iron^boots^boots^boots^need^to^gel^tablets^and^generals.
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem Clotting^boots^ingot 1
							RemoveThisItem General^combat^boots 1
							add_Item Soul^of^fight^iron^boots 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^boots^the^material.
						return
					endif
				else
					dlg 1 Clotting^boots^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 Create^soul^needs^to^gel^wrist^iron^ingots^and^generals^with^an^iron^fist.
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem Gel^Wrist^ingot 1
							RemoveThisItem General^iron^fist 1
							add_Item Soul^with^an^iron^fist 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^the^material^with^an^iron^fist.
						return
					endif
				else
					dlg 1 Gel^Wrist^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
	
	if Profession == 0
	$ask4 = ask You^need^to^make^is^which^part^of^General^equipment? Weapons Swordsman^leather^armor Swordsman^leather^shoulder Knight^helmet Knight^boots Swordsman^leather^wrist^watch Depart
		if $ask4 == 0
			if $sw3 > 5399
			$ask5 = ask What^is^the^weapon^you^want^to^create? Blood^Blade^(knife) Bone^claws^(claw) Depart
				if $ask5 == 0
					dlg 1 Blood^mist^blade^needs^to^gel^tablets^and^General^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Bloody^blade 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 Create^sharp^claws^need^gel^tablets^and^General^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Bone^claw 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif 
				
				if $ask5 == 2
					return
				else
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 Create^Knight^leather^armor^requires^ingots^and^coagulation^and^the^soul^armor.
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem Gel^nail^ingot 1
							RemoveThisItem General^mail 1
							add_Item Swordsman^leather^armor 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^armor^materials.
						return
					endif
				else
					dlg 1 Coagulation^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 Create^Knight^leather^shoulder^needs^iron^shoulder^Frozen^Shoulder^tabs^and^generals.
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem Frozen^Shoulder^tabs 1
							RemoveThisItem General^iron^shoulders 1
							add_Item Swordsman^leather^shoulder 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 General^you^lack^iron^shoulder^the^material.
						return
					endif
				else
					dlg 1 Frozen^Shoulder^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 599
				dlg 1 Create^Knight^helmets^need^helmets^ingot^coagulation^and^general^war^helmets.
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem Gel^helmet^ingot 1
							RemoveThisItem General^war^helmet 1
							add_Item Knight^helmet 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^lack^of^General^war^helmet^of^the^material.
						return
					endif
				else
					dlg 1 You^gel^helmet^ingot^is^missing^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 199
				dlg 1 Create^Knight^boots^boots^boots^need^to^gel^tablets^and^generals.
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem Clotting^boots^ingot 1
							RemoveThisItem General^combat^boots 1
							add_Item Knight^boots 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^boots^the^material.
						return
					endif
				else
					dlg 1 Clotting^boots^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 Create^Knight^leather^wrist^need^Gel^Wrist^iron^ingots^and^generals.
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem Gel^Wrist^ingot 1
							RemoveThisItem General^iron^fist 1
							add_Item Swordsman^leather^wrist^watch 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^the^material^with^an^iron^fist.
						return
					endif
				else
					dlg 1 Gel^Wrist^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
	
	if Profession == 3
	$ask4 = ask You^need^to^make^is^which^part^of^General^equipment? Staff Pro^day^robe Provisional^days^of^shoulder P-day^Cap Provisional^days^of^boots Provisional^days^of^wrist Depart
		if $ask4 == 0
			if $sw3 > 5399
				$ask5 = ask What^do^you^want^to^build^staff^is? Ancient^Maple^staff^(fire) God^wand^(ice) Depart
				if $ask5 == 0
					dlg 1 Create^ingots^of^ancient^Maple^staff^need^to^pour^soldiers^and^generals^fighting^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item Maple^wand 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 1
					dlg 1 Building^gods^staffs^need^to^gel^tablets^and^General^gun.
					if $item8 > 0
						if $item2 > 0
							if $n > 0
								RemoveThisItem Gel^tablets 1
								RemoveThisItem General^war^gun 1
								add_Item God^wand 1
								return
							else
								dlg 1 You^don't^have^enough^bag^space.
								return
							endif
						else
							dlg 1 Your^general^lack^of^gun^the^material.
							return
						endif
					else
						dlg 1 Coagulation^ingot^is^missing^you^this^material.
						return
					endif
				else
				endif
				
				if $ask5 == 2
					return
				else
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 1
			if $sw3 > 1799
				dlg 1 Create^pro^robe^requires^ingots^and^coagulation^and^the^soul^armor.
				if $item9 > 0
					if $item3 > 0
						if $n > 0
							RemoveThisItem Gel^nail^ingot 1
							RemoveThisItem General^war^gun 1
							add_Item Pro^day^robe 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^armor^materials.
						return
					endif
				else
					dlg 1 Coagulation^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 2
			if $sw3 > 1799
				dlg 1 Create^heaven^shoulder^needs^iron^shoulder^Frozen^Shoulder^tabs^and^generals.
				if $item10 > 0
					if $item4 > 0
						if $n > 0
							RemoveThisItem Frozen^Shoulder^tabs 1
							RemoveThisItem General^iron^shoulders 1
							add_Item Provisional^days^of^shoulder 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 General^you^lack^iron^shoulder^the^material.
						return
					endif
				else
					dlg 1 Frozen^Shoulder^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 3
			if $sw3 > 1799
				dlg 1 Create^heaven^helmet^Hat^gel^tablets^and^the^General^war^helmets.
				if $item11 > 0
					if $item5 > 0
						if $n > 0
							RemoveThisItem Gel^helmet^ingot 1
							RemoveThisItem General^war^helmet 1
							add_Item P-day^Cap 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^lack^of^General^war^helmet^of^the^material.
						return
					endif
				else
					dlg 1 You^gel^helmet^ingot^is^missing^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 4
			if $sw3 > 599
				dlg 1 Create^heaven^boots^boots^boots^need^to^gel^tablets^and^generals.
				if $item12 > 0
					if $item6 > 0
						if $n > 0
							RemoveThisItem Clotting^boots^ingot 1
							RemoveThisItem General^combat^boots 1
							add_Item Provisional^days^of^boots 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^of^boots^the^material.
						return
					endif
				else
					dlg 1 Clotting^boots^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			if $sw3 > -1
				dlg 1 Build^wrist^need^Gel^Wrist^iron^ingots^and^generals^of^heaven.
				if $item13 > 0
					if $item7 > 0
						if $n > 0
							RemoveThisItem Gel^Wrist^ingot 1
							RemoveThisItem General^iron^fist 1
							add_Item Provisional^days^of^wrist 1
							return
						else
							dlg 1 You^don't^have^enough^bag^space.
							return
						endif
					else
						dlg 1 Your^general^lack^the^material^with^an^iron^fist.
						return
					endif
				else
					dlg 1 Gel^Wrist^ingot^is^missing^you^this^material.
					return
				endif
			else
				dlg 1 Your^reputation^is^not,^cannot^be^forged.
				return
			endif
		else
		endif
		
		if $ask4 == 5
			return
		else
		endif
	else
	endif
proc_end
