//Spirit^of^wisdom

proc_start 1
var $item1
var $item2
var $item3

$item1 = havethisitem The^soul^of^courage
$item2 = havethisitem Spirit^of^wisdom
$item3 = havethisitem Power^of^soul

if $item1 > 0
	if $item2 > 0
		if $item3 > 0
			RemoveThisItem The^soul^of^courage 1
			RemoveThisItem Spirit^of^wisdom 1
			RemoveThisItem Power^of^soul 1
			add_Item General^soul^fragments 1
			return
		else
		endif
	else
	endif
else
endif
proc_end
