proc_start 1
	var $v1
	var $ask1
	
	$v1 = Can_get_new_quest
	if $v1 == 0
		dlg 1 Tasks^that^you^are^too^much!
		NpcStartMove
		return
	else
	endif
		
	dlg 0 Went^a^few^days^ago^of^the^ancient^battlefield^Warrior^shrine^said^neng^invented^NET^spirit^of^jade^can^clean^all^dirty^things,^think,^people^contracted^the^dirty^property^will^only^have^an^incurable^disease,^net^may^purify^soul^jade^ward^off^illness.
	$ask1 = ask Ancient^battlefield^taozhu^businessman^door^tieli^in^the^Valley,^making^purification^Valley^ghost^spar^NET^soul^jade^#--{#FFFF0000=}.^You^went^looking^for^him^to^buy^a^back^to^me,^I^treat^patients^immediately! Accept Refuse
	if $ask1 == 0
		set_quest_state 4500 1
		NpcStartMove
		return
  else
	NpcStartMove
  endif
proc_end

proc_start 2
	var $ask2
	dlg 0 Doctor^Yuan^far?^He^said^my^net^{#FFFF0000=^#}^can^purify^soul^jade^ward^off^disease?^Haha,^but^I^cannot^say^for^sure^thing!^But^I^am^sure^that^these^stones^can^gradually^dissolve^in^the^ancient^battlefield^of^the^corpses^of^the^grievances^of^the^ghost.
	$ask2 = ask Now^that^he's^willing^to^try^this^net^soul^jade^#^{#FFFF0000=}^magic^you^take^copper^#^{#FFFF0000=500}^to^him! Buy Refuse
	if $ask2 == 0
		if money > 499
			money -= 500
			add_Item Soul^NET^jade 1
			set_quest_state 4500 2
			return
		else
			dlg 1 You^don't^have^enough^money.
			return
		endif
  else
  endif		
proc_end

proc_start 3
	var $item1
	var $v2
	$item1 = havethisitem Soul^NET^jade
	$v2 = Get_quest_state 4500
	if $v2 == 2
		if $item1 > 0
			dlg 1 Well^...^...^{#FFFF0000=^#}^NET^soul^jade^reveals^a^magical^power.
			removethisitem Soul^NET^jade 1
			done_quest 4500
			NpcStartMove
			return
		else
			dlg 1 Tieli^hied^himself^to^the^ancient^battlefield^of^Tao^Zhu^businessman^bought^NET^soul^jade^#^{#FFFF0000=}^back!
			NpcStartMove
			return
		endif
	else
		dlg 1 Tieli^hied^himself^to^the^ancient^battlefield^of^Tao^Zhu^businessman^bought^NET^soul^jade^#^{#FFFF0000=}^back!
		NpcStartMove
		return
	endif
proc_end
