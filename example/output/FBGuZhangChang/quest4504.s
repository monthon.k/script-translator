//Magic^and^legends
proc_start 1
	var $v1
	var $ask1
	
	$v1 = Can_get_new_quest
	if $v1 == 0
		dlg 1 Tasks^that^you^are^too^much!
		return
	else
	endif
		
	dlg 0 God^guards^armed^with^weapon^fragments,^probably^fragments^of^ancient^magic^that^year.^Lakes^also^have^access^to^all^here,^to^pick^up^the^pieces,^will^be^used^by^God^to^build^out^God.^Soldiers^of^God^appear^blow^debris^scattered^about^the^ghost^Wraith^and^fear,^more^and^more^people^are^beginning^to^want^to^enter^the^ancient^battlefield^of^mystery^Valley,^hope^lucky^to^find^even^a^fragment^of^God^...^...
	$ask1 = ask Also^looking^for^the^pieces^as^shown^in,^trying^to^find^the^lost^magic,^the^comfort^God.^God^will^you^guard^the^drops^pieces^of^weapons^to^him,^do^not^know^if^this^is^the^legendary^magic^pieces. Accept Refuse
	if $ask1 == 0
		set_quest_state 4504 1
		return
  else
  endif
proc_end

proc_start 2
		var $item1
		$item1 = havethisitem Weapon^fragment
		if $item1 > 19
			dlg 0 These^fragments^reveals^a^resentment,^maybe^is^the^legendary^magic^pieces.^God^is^God^of^all^weapons,^one^of^God's^long^cold^dagger,^waterfalls,^ancient^Lord^Chiyou^General^demon^under^empty^and^all.^This^dagger^even^five-inch-long,^wide^and^pointed,^the^entire^body^is^transparent,^on^the^dagger^handle^side^part^carved^with^the^legend^of^the^Dragon's^nine^sons:^pepper^map,^curved^air,^chiwen,^Po^prison,^prisoners^cattle,^suanni,^bi^...^...
			dlg 1 These^fierce^countenance,^ferocious^dragons,^make^like^a^raw^floating^router^ends^the^knife^handle,^as^if^guarding^what^you^or^cursing^General,^surrounded^by^less^than^two-inch^handles.^This^dagger^and^chilly,^even^hot^weather,^this^dagger^will^carry,^can^also^make^people^feel^the^heat^totally,^entirely^at^ease,^it^is^also^called^cold^evil^dragon^dagger.
			removethisitem Weapon^fragment 20
			done_quest 4504
			return
		else
			dlg 1 Complete^Bu^Ping^trust^collection^gods^will^guard^the^20^weapon^drops^pieces!
			return
		endif
proc_end
