proc_start 0
proc_end

proc_start 1
SetDialogName Skill^books
var $ask1
Var $n
$n = CanLearnSkill 1044 1
var $m
$m = IsThisSkillActived 1044 1
	if $m == 1
		dlg 1 You^have^learned^the^skills.
		return
	else
	endif
	
	if Profession == 0
	$ask1 = ask Do^you^want^to^learn^this^skill Yes No
		if $ask1 == 0
			if $n == 1
				addskill 1044
				removethisitem Scroll^of^life 1
				PlayMusic 0 DATA\Sound\UI\NewSkill.wav
				return
			else
				dlg 1 You^don't^have^to^meet^the^conditions^of^learning.
				return
			endif
		else
			return
		endif
	else
		dlg 1 Your^career^don't^meet^the^conditions^for^learning.
		return
	endif
proc_end
