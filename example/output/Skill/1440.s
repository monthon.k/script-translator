proc_start 0
proc_end

proc_start 1
SetDialogName Skill^books
var $ask1
Var $n
$n = CanLearnSkill 1440 1
var $m
$m = IsThisSkillActived 1440 1
	if $m == 1
		dlg 1 You^have^to^learn^the^skills.
		return
	else
	endif
	
	if Profession == 4
	$ask1 = ask If^you^want^to^learn^this^skill Yeah Whether
		if $ask1 == 0
			if $n == 1
				addskill 1440
				removethisitem Scroll^of^sacrifice 1
				PlayMusic 0 DATA\Sound\UI\NewSkill.wav
				return
			else
				dlg 1 You^do^not^meet^the^conditions.
			endif
		else
			return
		endif
	else
		dlg 1 Your^career^does^not^meet^the^conditions^of^learning
		return
	endif
proc_end
