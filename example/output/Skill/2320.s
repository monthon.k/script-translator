//Action Reel - hand

proc_start 0
proc_end

proc_start 1
SetDialogName Action^reel
var $item1
$item1 = havethisitem Scroll-hand
if $item1 > 0
else
	return
endif
var $ask1
Var $n
$n = CanLearnSkill 2320 1
var $m
$m = IsThisSkillActived 2320 1
	if $m == 1
		dlg 1 You^have^learned^the^action.
		return
	else
	endif
	
//	if Profession == 3
	$ask1 = ask Do^you^want^to^learn^this^action Yes No
		if $ask1 == 0
			if $n == 1
				removethisitem Scroll-hand 1
				addskill 2320
				return
			else
				dlg 1 You^did^not^meet^the^conditions^or^have^learned^the^movements
				return
			endif
		else
			return
		endif
//	else
//		dlg 1 Your^career^does^not^meet^the^conditions^of^learning
//		return
//	endif
proc_end
