
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Monthon
 */
public class lineProcess {
    String line;
    String translatedLine;
    int cur;
    String lastToken;
    boolean isLoadItem;
    boolean isLoadNpc;
    ArrayList<String> clistItem;
    ArrayList<String> elistItem;
    ArrayList<String> clistNpc;
    ArrayList<String> elistNpc;
    Mode translateMode;
    
    public enum Mode
    {
        ITEM, DIALOG, COMMAND
    }
    public lineProcess(String s)
    {
        line=s;
        cur=0;
        translatedLine="";
        lastToken="";
        isLoadItem=false;
        isLoadNpc=false;
        clistItem=new ArrayList<String>();
        elistItem=new ArrayList<String>();
        clistNpc=new ArrayList<String>();
        elistNpc=new ArrayList<String>();
    }
    
    public boolean hasNext()
    {
        boolean bHas=false;
        for(int i=cur; !bHas && i<line.length();i++)
        {
            if(line.charAt(i)!=' ' && line.charAt(i)!='\t')
            {
                bHas=true;
                cur=i;
            }
            else 
            {
                translatedLine+=line.charAt(i);
            }
        }
        return bHas;
    }
    
    public void testGit()
    {
        System.out.println("New method for Git testing");
    }
    
    public String nextToken()
    {
        String token="";
        
        char tmp;
        while( cur<line.length() && (tmp=line.charAt(cur)) != ' ' && tmp != '\t')
        {
            token+=tmp;
            cur++;
        }
        switch(lastToken)
        {
            case "removethisitem" :
                translateMode=Mode.ITEM;
                break;
            case "add_item" :
                translateMode=Mode.ITEM;
                break;
            case "havethisitem" :
                translateMode=Mode.ITEM;
                break;
            case "Item" :
                translateMode=Mode.ITEM;
                break;
            default :
                translateMode=Mode.DIALOG;
        }
        
        if(translateMode != Mode.ITEM)
        {
            switch(token)
            {
                case "proc_start" :
                    translateMode=Mode.COMMAND;
                    break;
                case "proc_end" :
                    translateMode=Mode.COMMAND;
                    break;
                case "add_item" :
                    translateMode=Mode.COMMAND;
                    break;
                case "removethisitem" :
                    translateMode=Mode.COMMAND;
                    break;
                case "havethisitem" :
                    translateMode=Mode.COMMAND;
                    break;
                case "var" :
                    translateMode=Mode.COMMAND;
                    break;
                case "money" :
                    translateMode=Mode.COMMAND;
                    break;
                case "tell" :
                    translateMode=Mode.COMMAND;
                    break;
                case "if" :
                    translateMode=Mode.COMMAND;
                    break;
                case "else" :
                    translateMode=Mode.COMMAND;
                    break;
                case "endif" :
                    translateMode=Mode.COMMAND;
                    break;
                case "return" :
                    translateMode=Mode.COMMAND;
                    break;
                case "silver" :
                    translateMode=Mode.COMMAND;
                    break;
                case "==" :
                    translateMode=Mode.COMMAND;
                    break;
                case "jump" :
                    translateMode=Mode.COMMAND;
                    break;
                case "ask" :
                    translateMode=Mode.COMMAND;
                    break;
                case "set_quest_state" :
                    translateMode=Mode.COMMAND;
                    break;
                case "=" :
                    translateMode=Mode.COMMAND;
                    break;
                case ">" :
                    translateMode=Mode.COMMAND;
                    break;
                case "level" :
                    translateMode=Mode.COMMAND;
                    break;
                case "get_quest_state" :
                    translateMode=Mode.COMMAND;
                    break;
                case "dlg" :
                    translateMode=Mode.COMMAND;
                    break;
                case "done_quest" :
                    translateMode=Mode.COMMAND;
                    break;
                case "flytomap" :
                    translateMode=Mode.COMMAND;
                    break;
                case "Talk" :
                    translateMode=Mode.COMMAND;
                    break;
                case "getrideid" :
                    translateMode=Mode.COMMAND;
                    break;
                case "showintonatebar" :
                    translateMode=Mode.COMMAND;
                    break;
                case "doride" :
                    translateMode=Mode.COMMAND;
                    break;
                case "useskill" :
                    translateMode=Mode.COMMAND;
                    break;
                case "can_get_new_quest" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Name]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Intro]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[PlayerLevelMin]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[PlayerLevel]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[PlayerLevelMax] " :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Quest]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Level]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Repeat]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Reward]" :
                    translateMode=Mode.COMMAND;
                    break;  
                case "[Show]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[State]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[NextStateNeed]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "[Reputation]" :
                    translateMode=Mode.COMMAND;
                    break;
                case "Money" :
                    translateMode=Mode.COMMAND;
                    break;
                case "skillexp" :
                    translateMode=Mode.COMMAND;
                    break;
                case "Item" :
                    translateMode=Mode.COMMAND;
                    break;
                default :
                    translateMode=Mode.DIALOG;
            }
            
            if(isInteger(token))
                translateMode=Mode.COMMAND;
            if(token.charAt(0)=='$')
                translateMode=Mode.COMMAND;
        }
        
        if( token.length() >= 2 && token.substring(0, 2).equals("//"))
        {
            token=token.substring(2);
            translatedLine+="//";
        }
        
//        String temp="";
//        if(token.indexOf('"')>=0)
//        {
//            translateMode=Mode.DIALOG;
//            translatedLine+=token.substring(0, token.indexOf('"')+1);
//            temp=token.substring(token.lastIndexOf('"'));
//            token=token.substring(token.indexOf('"'), token.lastIndexOf('"')+1);
//        }
//        else
//        {
//            translateMode=Mode.COMMAND;
//        }
        switch(translateMode)
        {
            case ITEM :
                //translatedLine += "translateitem";
                translatedLine += translateItem(token);
                break;
            case DIALOG :
                //translatedLine += "translatedialog"; 
                translatedLine += translateDialog(token);
                //translatedLine += translateDialog(token)+temp;
                break;
            case COMMAND :
                translatedLine += token;
                break;
        }
        lastToken=token;
        return token;
    }
    
    public boolean isInteger( String input ) {
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
    
    private String translateDialog(String dialog)
    {
        //System.out.println("dialog mode:"+dialog);
        BufferedReader br = null;
        
        Translate.setClientId("sotranslatorid");
        Translate.setClientSecret("9lVGbVSaPclYakqaBIEy8QfWTAC+B3yyphKEdBQ+aZk=");
        if(!isLoadNpc)
        {
            String tmp;
            try 
            {
                br = new BufferedReader(new InputStreamReader(new FileInputStream("ch_npc.txt"), "GB18030"));
                while((tmp=br.readLine())!=null)
                {
                    //System.out.println(tmp);
                    clistNpc.add(tmp);
                }
            
                br = new BufferedReader(new InputStreamReader(new FileInputStream("eng_npc.txt"), "GB18030"));
                while((tmp=br.readLine())!=null)
                {
                    elistNpc.add(tmp);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            } 
            isLoadNpc=true;
                //System.out.println("Loaded");
       }
       String result=dialog;
       String ch, eng;
       for(int i=0;i<clistNpc.size();i++)
       {
           ch=clistNpc.get(i);
           eng=elistNpc.get(i).replace(' ', '^');
           //System.out.println(eng);
            if(result.contains(ch))
            {
                result.replace(ch, eng);
            }
       }
       try  {
            result = Translate.execute(result, Language.CHINESE_SIMPLIFIED, Language.ENGLISH);
       } catch (Exception ex) {
            Logger.getLogger(scriptFrame.class.getName()).log(Level.SEVERE, null, ex);
       } 
       result=result.replace(' ', '^');
       return result;
    }
    
    private String translateItem(String item)
    {
        BufferedReader br = null;
        //System.out.println("item mode:" + item);
        if(!isLoadItem)
        {
            String tmp;
            try 
            {
                br = new BufferedReader(new InputStreamReader(new FileInputStream("chinese.txt"), "GB18030"));
                while((tmp=br.readLine())!=null)
                {
                    //System.out.println(tmp);
                    clistItem.add(tmp);
                }
            
                br = new BufferedReader(new InputStreamReader(new FileInputStream("english.txt"), "GB18030"));
                while((tmp=br.readLine())!=null)
                {
                    elistItem.add(tmp);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            } 
            isLoadItem=true;
                //System.out.println("Loaded");
       }
       String result=item;
       for(int i=0;i<clistItem.size();i++)
       {
           //System.out.println("clist:"+clistItem.get(i));
            if(clistItem.get(i).equals(result))
            {
                //System.out.println(elistItem.get(i));
                result=elistItem.get(i);
                i=clistItem.size();
            }
       }
       result=result.replace(' ', '^');
       return result;
    }
    
    public String getTranslatedLine()
    {
        return translatedLine;
    }
}
